package utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Оваа класа ги содржи сите utility методи поврзани со чекање
 */
public class WaitUtilities {

    public static final int EXPLICIT_WAIT_IN_SEC = 15;
    public static final int IMPLICIT_WAIT_IN_SEC = 10;


    /**
     * Чекај додека на Web Element-от може да се клика
     * @param element
     * @param driver
     */
    public void waitElementToBecomeClickable(final WebElement element, final WebDriver driver) {
        new WebDriverWait(driver, EXPLICIT_WAIT_IN_SEC)
                .until(ExpectedConditions.elementToBeClickable(element));
    }


    synchronized public void staticWait(long milliseconds) {
        try {
            TimeUnit.MILLISECONDS.wait(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * Чекај додека By елементот стане невидлив
     * @param locator
     * @param driver
     */
    public void waitElementToBecomeInvisible(By locator, WebDriver driver) {
        new WebDriverWait(driver, EXPLICIT_WAIT_IN_SEC)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }


    /**
     * Чекај додека By елементот стане присутен
     * @param locator
     * @param driver
     */
    public void waitElementToBecomePresent(By locator, WebDriver driver) {
        new WebDriverWait(driver, EXPLICIT_WAIT_IN_SEC)
                .until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    /**
     * Чекај By елементот да стане видлив
     * @param locator
     * @param driver
     */
    public void waitElementToBecomeVisible(By locator, WebDriver driver) {
        new WebDriverWait(driver, EXPLICIT_WAIT_IN_SEC)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
    }


    /**
     * Чекај Web елементот да стане видлив
     * @param element
     * @param driver
     */
    public void waitElementToBecomeVisible(WebElement element, WebDriver driver) {
        new WebDriverWait(driver, EXPLICIT_WAIT_IN_SEC)
                .until(ExpectedConditions.visibilityOf(element));
    }

    public void waitElementToBecomeDisplayed(WebElement element, WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, EXPLICIT_WAIT_IN_SEC);
        ExpectedCondition<Boolean> elementIsDisplayed = arg0 -> element.isDisplayed();
        wait.until(elementIsDisplayed);
    }

}
