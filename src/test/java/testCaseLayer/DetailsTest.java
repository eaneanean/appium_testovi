package testCaseLayer;

import org.aspectj.lang.annotation.Before;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageLayer.DetailsPageObject;
import pageLayer.MainActivityPageObject;

public class DetailsTest extends BasicSetup {

    private DetailsPageObject detailsPageObject;

    @BeforeMethod
    public void init() {
        detailsPageObject = new DetailsPageObject(driver);
    }

    @Test
    /**
     * Се проверува дали информациите што ги има и во главната Activity и во
     * Details Activity се исти(тоа се висока температура, ниска температура и опис
     * на временската состојба)
     */
    public void mainAndDetailsShouldBeSame() {

        MainActivityPageObject mainActivityPageObject  = new MainActivityPageObject(driver);

        String highTemperatureMenu = mainActivityPageObject.valueHighTemperature();
        String lowTemperatureMenu = mainActivityPageObject.valueLowTemperature();
        String weatherDescriptionMenu = mainActivityPageObject.valueWeatherDescription();

        String highTemperatureDetails = detailsPageObject.getHighTemperature();
        String lowTemperatureDetails = detailsPageObject.getLowTemperature();
        String weatherDescriptionDetails = detailsPageObject.getWeatherDescription();

        Assert.assertEquals(highTemperatureMenu, highTemperatureDetails);
        Assert.assertEquals(lowTemperatureMenu, lowTemperatureDetails);
        Assert.assertEquals(weatherDescriptionMenu, weatherDescriptionDetails);
    }






}
