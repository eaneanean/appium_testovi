package testCaseLayer;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.URL;


import static org.junit.jupiter.api.Assertions.assertFalse;

public class BasicSetup {

    AndroidDriver<AndroidElement> driver;

    public static final String DEVICE_NAME = "Xiaomi A2 Lite";
    public static final String PLATFORM_NAME = "Android";
    public static final boolean FULL_RESET = false;
    public static final boolean NO_RESET = false;
    public static final boolean AUTO_GRANT_PERMISSIONS = true;
    public static final String APPLICATION_LOCATION = "/home/ivan/AndroidStudioProjects/" +
            "MyApplication19 (copy)/app/build/outputs/apk/debug/app-debug.apk";
    public static final String APP_PACKAGE_NAME = "com.example.android.vremenska";
    public static final String PLATFORM_VERSION = "9.0";

    private final String SERVER_URL = "http://127.0.0.1:4723/wd/hub";



    @BeforeMethod
    public void setUp() throws Exception {

        DesiredCapabilities desiredCapabilities = setDesiredCapabilities();

        driver = new AndroidDriver<AndroidElement>(new URL(SERVER_URL), desiredCapabilities);
    }


    @Test
    public void test() {
        assertFalse(driver == null);
    }

    private DesiredCapabilities setDesiredCapabilities() {

        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, DEVICE_NAME);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, PLATFORM_NAME);
        capabilities.setCapability(MobileCapabilityType.FULL_RESET, FULL_RESET);
        capabilities.setCapability(MobileCapabilityType.NO_RESET, NO_RESET);
        capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, AUTO_GRANT_PERMISSIONS);
        capabilities.setCapability(MobileCapabilityType.APP, APPLICATION_LOCATION);
        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, APP_PACKAGE_NAME);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, PLATFORM_VERSION);

        return capabilities;
    }

    @AfterMethod
    public void quitDriver() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.out.println(e);
        }
    }




}
