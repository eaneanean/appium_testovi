package testCaseLayer;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageLayer.MainActivityPageObject;
import pageLayer.MenuPageObject;

import java.util.concurrent.TimeoutException;

public class MenuTest extends BasicSetup {

    private MenuPageObject menuPageObject;

    public MenuTest() {
        super();
    }

    @BeforeMethod
    public void init() {
        menuPageObject = new MenuPageObject(driver);
    }

    @Test(expectedExceptions = org.openqa.selenium.TimeoutException.class)
    public void actionBarShouldAppear() {

        Assert.assertTrue(menuPageObject.sizeMainActionBar() != 0);
        menuPageObject.clickMoreOptions();
        menuPageObject.clickMapLocation();

        //Ова значи дека сме излегле од нашата апликација, како што беше и намерата
        Assert.assertFalse(menuPageObject.sizeContent() != 0);
    }

    @Test
    public void menuActionBarShouldApper() {

        menuPageObject.clickMoreOptions();
        menuPageObject.clickSettingsItem();
        Assert.assertTrue(menuPageObject.sizeSettings() != 0);

        // врати се на претходната Main Activity
        menuPageObject.tapSettingsBackArrow();

        menuPageObject.clickMoreOptions();
        menuPageObject.clickSettingsItem();
        // дали повторно ќе се прикаже Action bar-от
        Assert.assertTrue(menuPageObject.sizeSettings() != 0);
    }

    @Test
    public void todayActionBarTitleShouldBeAsExpected() {

        menuPageObject.tapTodayWeatherDetails();
        Assert.assertEquals(menuPageObject.actionBarDetailsTitle(),
                menuPageObject.DETAILS_MENU_TITLE);

        // се враќаме на главното мени
        menuPageObject.tapSettingsBackArrow();
        menuPageObject.tapTodayWeatherDetails();

        Assert.assertEquals(menuPageObject.actionBarDetailsTitle(),
                menuPageObject.DETAILS_MENU_TITLE);

    }

    @Test
    public void tommorowActionBarTitleShouldBeAsExpected() {

        menuPageObject.clickTomorrow();
        Assert.assertEquals(menuPageObject.actionBarDetailsTitle(),
                menuPageObject.DETAILS_MENU_TITLE);

        //се враќаме на главното мени
        menuPageObject.tapSettingsBackArrow();
        menuPageObject.clickTomorrow();

        Assert.assertEquals(menuPageObject.actionBarDetailsTitle(),
                menuPageObject.DETAILS_MENU_TITLE);
    }

    @Test
    public void availableAppsForSharingShouldPopUp() {

        menuPageObject.tapTodayWeatherDetails();
        menuPageObject.clickShare();

        Assert.assertTrue(menuPageObject.getTextFromTitle().contains(menuPageObject.SHARE_WITH_TEXT));
    }







}


