package testCaseLayer;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageLayer.DetailsPageObject;
import pageLayer.MainActivityPageObject;
import pageLayer.MenuPageObject;
import utilities.WaitUtilities;

public class MainActivityTest extends BasicSetup {

    private MainActivityPageObject mainActivityPageObject;

    public MainActivityTest() {
        super();
    }

    @BeforeMethod
    public void init() {
        mainActivityPageObject = new MainActivityPageObject(driver);
    }


    @Test
    public void shouldDisplayMainContentView() {
        Assert.assertTrue(mainActivityPageObject.displayMainContent());
    }

    @Test
    public void shouldDisplayRecyclerView() {
        Assert.assertTrue(mainActivityPageObject.displayRecyclerView());
    }

    @Test
    public void shouldDisplayDate() {
        Assert.assertTrue(mainActivityPageObject.displayDateView());
    }

    @Test
    public void dateTest() {
        String text = mainActivityPageObject.valueMainWidgetDate();
        System.out.println("Date:" + text);
        Assert.assertNotNull(text);
    }

    @Test
    public void shouldDisplayIcon() {
        Assert.assertTrue(mainActivityPageObject.displayWeatherIcon());
    }

    @Test
    public void shouldNotBeNullHighTemperature() {
        Assert.assertNotNull(mainActivityPageObject.valueHighTemperature());
    }

    @Test
    public void shouldDisplayHighTemperature() {
        Assert.assertTrue(mainActivityPageObject.displayHighTemperature());
    }

    @Test
    public void shouldNotBeNullLowTemperature() {
        Assert.assertNotNull(mainActivityPageObject.displayLowTemperature());
    }

    @Test
    public void shouldDisplayLowTemperature() {
        Assert.assertTrue(mainActivityPageObject.displayLowTemperature());
    }

    @Test
    public void shouldDisplayWeatherDescription() {
        Assert.assertTrue(mainActivityPageObject.displayWeatherDescription());
    }

    @Test
    public void shouldNotBeNullWeatherDescription() {
        Assert.assertNotNull(mainActivityPageObject.valueWeatherDescription());
    }


    @Test
    public void shouldContainToday() {
        Assert.assertTrue(mainActivityPageObject.valueTodayView().toLowerCase().contains("today"));
    }

    @Test
    public void shouldContainTomorrow() {
        Assert.assertTrue(mainActivityPageObject.valueTomorrowView().toLowerCase().contains("tomorrow"));
    }

    @Test
    /**
     * Се забележува температурата, се притиснува на некој друг ден и се проверува
     * дали температурата останала иста
     */
    public void temperatureShouldStaySame() {


        MenuPageObject menuPageObject = new MenuPageObject(driver);
        // температра ја добиваме во формат: бројката, следена со знакот за степени и целта на
        // подолниот код е да останат само нумеричките вредности на стрингот
        int temperatureBeforeTomorrow = Integer.parseInt(mainActivityPageObject.valueHighTemperature().
                substring(0, mainActivityPageObject.valueHighTemperature().length() - 1));

          menuPageObject.clickTomorrow();
          menuPageObject.tapSettingsBackArrow();

        // истата операција како и онаа изврешна малку погоре
        int temperatureAfterTommorow = Integer.parseInt(mainActivityPageObject.valueHighTemperature().
                substring(0, mainActivityPageObject.valueHighTemperature().length() - 1));

        Assert.assertEquals(temperatureBeforeTomorrow, temperatureAfterTommorow);
    }

}
