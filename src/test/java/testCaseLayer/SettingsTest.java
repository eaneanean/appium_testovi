package testCaseLayer;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageLayer.MainActivityPageObject;
import pageLayer.MenuPageObject;
import pageLayer.SettingsPageObject;

public class SettingsTest extends BasicSetup {

    private SettingsPageObject settingsPageObject;

    @BeforeMethod
    public void init() {
        this.settingsPageObject = new SettingsPageObject(driver);
    }


    @Test
    public void shouldDisplayTemperatureUnits() {

        MenuPageObject menuPageObject = new MenuPageObject(driver);

        menuPageObject.clickMoreOptions();
        menuPageObject.clickSettingsItem();
        settingsPageObject.clickTemperatureUnits();


        Assert.assertEquals(settingsPageObject.getAlertTitle(),
                menuPageObject.ALERT_TITLE);

    }

    @Test
    public void temperatureShouldStaySame() {

        MenuPageObject menuPageObject = new MenuPageObject(driver);
        MainActivityPageObject mainActivityPageObject = new MainActivityPageObject(driver);
        // температра ја добиваме во формат: бројката, следена со знакот за степени и целта на
        // подолниот код е да останат само нумеричките вредности на стрингот
        int temperatureMetric = Integer.parseInt(mainActivityPageObject.valueHighTemperature().
                substring(0, mainActivityPageObject.valueHighTemperature().length() - 1));

        menuPageObject.clickMoreOptions();
        menuPageObject.clickSettingsItem();
        settingsPageObject.clickTemperatureUnits();
        Assert.assertEquals(true, true);
        settingsPageObject.clickImperial();

        menuPageObject.tapSettingsBackArrow();



        // истата операција како и онаа изврешна малку погоре
        int temperatureImperial = Integer.parseInt(mainActivityPageObject.valueHighTemperature().
                substring(0, mainActivityPageObject.valueHighTemperature().length() - 1));

        int lowThreshold = (int) (temperatureMetric * 1.8) + 32 - 1;
        int highThreashold = (int) (temperatureMetric * 1.8) + 32 + 1;

        boolean isBetweenThreshold = temperatureImperial >= lowThreshold &&
                                    temperatureImperial <= highThreashold;

        Assert.assertEquals(true, isBetweenThreshold);
    }

}
