package pageLayer;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import testCaseLayer.BasicSetup;
import utilities.WaitUtilities;

import java.time.Duration;

public class SettingsPageObject extends BasicSetupPageObject {

    public SettingsPageObject(AppiumDriver appiumDriver) {
        super(appiumDriver);
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(WaitUtilities.IMPLICIT_WAIT_IN_SEC)), this);
    }

    public static final String ALERT_TITLE = "Temperature Units";

    @AndroidFindBy(id = BasicSetup.APP_PACKAGE_NAME + ":id/alertTitle")
    AndroidElement alertTitle;

    public String getAlertTitle() {
        waitUtilities.waitElementToBecomeDisplayed(alertTitle, driver);
        return alertTitle.getText();
    }



    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
            "android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]")
    AndroidElement temperatureUnits1;


    public void clickTemperatureUnits() {
        waitUtilities.waitElementToBecomeDisplayed(temperatureUnits1, driver);
        System.out.println("Temperature units are displayed");
        temperatureUnits1.click();
    }

    @AndroidBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/" +
            "android.support.v7.widget.LinearLayoutCompat/android.widget.FrameLayout/" +
            "android.widget.ListView/android.widget.CheckedTextView[1]")
    AndroidElement metric;

    public void clickMetric() {
        waitUtilities.waitElementToBecomeDisplayed(metric, driver);
        metric.click();
    }

    public boolean isMetricSelected() {
        return metric.isSelected();
    }

    @AndroidBy(xpath = "//hierarchy/android.widget.FrameLayout/" +
            "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/" +
            "android.widget.FrameLayout/android.support.v7.widget.LinearLayoutCompat/" +
            "android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[2]")
    AndroidElement imperial;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[2]")
    AndroidElement imperial1;

    public void clickImperial() {
        waitUtilities.waitElementToBecomeDisplayed(imperial1, driver);
        imperial1.click();
    }

    public boolean isImperialSelected() {
        return imperial.isDisplayed();
    }




}
