package pageLayer;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import utilities.WaitUtilities;

import java.time.Duration;

public class BasicSetupPageObject {


    AppiumDriver driver;
    WaitUtilities waitUtilities;

    public BasicSetupPageObject(AppiumDriver driver) {
        this.driver = driver;
        waitUtilities = new WaitUtilities();
    }
}
