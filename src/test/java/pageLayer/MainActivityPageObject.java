package pageLayer;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import testCaseLayer.BasicSetup;
import utilities.WaitUtilities;

import java.time.Duration;

public class MainActivityPageObject extends BasicSetupPageObject{

    private final String PROJECT_PACKAGE = BasicSetup.APP_PACKAGE_NAME;

    @AndroidFindBy(id="android:id/content")
    AndroidElement content;

    @AndroidFindBy(id = PROJECT_PACKAGE + ":id/recyclerview_forecast")
    AndroidElement recyclerView;



    public MainActivityPageObject(AppiumDriver driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(WaitUtilities.IMPLICIT_WAIT_IN_SEC)), this);
    }

    public boolean displayMainContent() {
        waitUtilities.waitElementToBecomeDisplayed(content, driver);
        return content.isDisplayed();
    }

    public boolean displayRecyclerView() {
        waitUtilities.waitElementToBecomeDisplayed(recyclerView, driver);
        return recyclerView.isDisplayed();
    }


    @AndroidFindBy(id = PROJECT_PACKAGE + ":id/date")
    WebElement dateView;

    public boolean displayDateView() {
        waitUtilities.waitElementToBecomeDisplayed(dateView, driver);
        return recyclerView.isDisplayed();
    }


    public String valueMainWidgetDate() {
        waitUtilities.waitElementToBecomeDisplayed(dateView, driver);
        return dateView.getText();

    }

    @AndroidFindBy(id = PROJECT_PACKAGE + ":id/weather_icon")
    WebElement weatherIcon;

    public boolean displayWeatherIcon() {
        waitUtilities.waitElementToBecomeDisplayed(weatherIcon, driver);
        return weatherIcon.isDisplayed();
    }

    @AndroidFindBy(id = PROJECT_PACKAGE + ":id/high_temperature")
    WebElement highTemperature;

    public boolean displayHighTemperature() {
        waitUtilities.waitElementToBecomeDisplayed(highTemperature, driver);
        return highTemperature.isDisplayed();
    }

    public String valueHighTemperature() {
        waitUtilities.waitElementToBecomeDisplayed(highTemperature, driver);
        System.out.println("High temperature is displayed");
        return highTemperature.getText();
    }

    @AndroidFindBy(id = PROJECT_PACKAGE + ":id/low_temperature")
    WebElement lowTemperature;
    public boolean displayLowTemperature() {
        waitUtilities.waitElementToBecomeDisplayed(lowTemperature, driver);
        return lowTemperature.isDisplayed();
    }

    public String valueLowTemperature() {
        waitUtilities.waitElementToBecomeDisplayed(lowTemperature, driver);
        return lowTemperature.getText();
    }


    @AndroidFindBy(id = PROJECT_PACKAGE + ":id/weather_description")
    WebElement weatherDescription;
    public boolean displayWeatherDescription() {
        waitUtilities.waitElementToBecomeDisplayed(weatherDescription, driver);
        return weatherDescription.isDisplayed();
    }

    public String valueWeatherDescription() {
        waitUtilities.waitElementToBecomeDisplayed(weatherDescription, driver);
        return weatherDescription.getText();
    }

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/" +
            "android.widget.FrameLayout/android.support.v7.widget." +
            "RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[1]")
    WebElement todayView;

    public String valueTodayView() {
        waitUtilities.waitElementToBecomeDisplayed(todayView, driver);
        return todayView.getText();
    }


    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/" +
            "android.widget.FrameLayout/android.support.v7.widget.RecyclerView/" +
            "android.view.ViewGroup[2]/android.widget.TextView[1]")
    WebElement tomorrowView;

    public String valueTomorrowView() {
        waitUtilities.waitElementToBecomeDisplayed(tomorrowView, driver);
        return tomorrowView.getText();
    }

    @AndroidFindBy(id = "34daaeef-9948-4787-aea2-3c7cab2c035f")
    AndroidElement temp;

    public String getTempText() {
        waitUtilities.waitElementToBecomeDisplayed(temp, driver);
        return temp.getText();
    }

}
