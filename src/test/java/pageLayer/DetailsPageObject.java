package pageLayer;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import testCaseLayer.BasicSetup;
import utilities.WaitUtilities;

import java.time.Duration;

public class DetailsPageObject extends BasicSetupPageObject {

    public DetailsPageObject(AppiumDriver driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver,
                Duration.ofSeconds(WaitUtilities.IMPLICIT_WAIT_IN_SEC)), this);
    }


    @AndroidFindBy(id = BasicSetup.APP_PACKAGE_NAME + ":id/date")
    AndroidElement date;

    public String getDateText() {
        waitUtilities.waitElementToBecomeDisplayed(date, driver);
        return date.getText();
    }

    @AndroidFindBy(id = BasicSetup.APP_PACKAGE_NAME + ":id/high_temperature")
    AndroidElement highTemperature;

    public String getHighTemperature() {
        waitUtilities.waitElementToBecomeDisplayed(highTemperature, driver);
        return highTemperature.getText();
    }


    @AndroidFindBy(id = BasicSetup.APP_PACKAGE_NAME + ":id/low_temperature")
    AndroidElement lowTemperature;

    public String getLowTemperature() {
        waitUtilities.waitElementToBecomeDisplayed(lowTemperature, driver);
        return lowTemperature.getText();
    }

    @AndroidFindBy(id = BasicSetup.APP_PACKAGE_NAME + ":id/weather_description")
    AndroidElement weatherDescription;

    public String getWeatherDescription() {
        waitUtilities.waitElementToBecomeDisplayed(weatherDescription, driver);
        return weatherDescription.getText();
    }

}
