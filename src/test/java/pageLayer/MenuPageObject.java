package pageLayer;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import testCaseLayer.BasicSetup;
import utilities.WaitUtilities;

import java.time.Duration;


public class MenuPageObject extends BasicSetupPageObject {

    public static final String DETAILS_MENU_TITLE = "Details";
    public static final String SHARE_WITH_TEXT = "Share with";
    public static final String ALERT_TITLE = "Temperature Units";

    @AndroidFindBy(id = BasicSetup.APP_PACKAGE_NAME + ":id/action_bar")
    AndroidElement mainActionBar;

    public int sizeMainActionBar() {
        waitUtilities.waitElementToBecomeVisible(mainActionBar, driver);
        return driver.findElementsById(BasicSetup.APP_PACKAGE_NAME + ":id/action_bar").size();
    }

    public MenuPageObject(AppiumDriver appiumDriver) {
        super(appiumDriver);
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(WaitUtilities.IMPLICIT_WAIT_IN_SEC)), this);
    }

    @AndroidFindBy(accessibility = "More options")
    AndroidElement actionBarMoreOptions;

    public void clickMoreOptions() {
        waitUtilities.waitElementToBecomeClickable(actionBarMoreOptions, driver);
        actionBarMoreOptions.click();
    }

    @AndroidFindBy(accessibility = "Share")
    AndroidElement shareItemActionBar;

    public void clickShare() {
        waitUtilities.waitElementToBecomeDisplayed(shareItemActionBar, driver);
        shareItemActionBar.click();
    }


    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget" +
            ".FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget" +
            ".LinearLayout/android.widget.RelativeLayout/android.widget.TextView")
    AndroidElement actionBarMapLocation;

    public void clickMapLocation() {
        waitUtilities.waitElementToBecomeClickable(actionBarMapLocation, driver);
        actionBarMapLocation.click();
    }

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget" +
            ".FrameLayout/android.widget.ListView/android.widget.LinearLayout[2]")
    AndroidElement actionBarSettings;

    public void clickSettingsItem() {
        waitUtilities.waitElementToBecomeDisplayed(actionBarMapLocation, driver);
        actionBarSettings.click();
    }

    @AndroidFindBy(accessibility = "Navigate up")
    AndroidElement actionBarBackArrow;

    public void tapSettingsBackArrow() {
        waitUtilities.waitElementToBecomeClickable(actionBarBackArrow, driver);
        actionBarBackArrow.click();
    }



    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]")
    AndroidElement todayWeatherDetails;


    public void tapTodayWeatherDetails() {
        waitUtilities.waitElementToBecomeClickable(todayWeatherDetails, driver);
        todayWeatherDetails.click();
    }

    @AndroidFindBy(xpath = "/hierarchy/android.widget" +
            ".FrameLayout/android.widget.LinearLayout/android.widget" +
            ".FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android" +
            ".view.ViewGroup/android.widget.TextView")
    AndroidElement actionBarDetailsTitle;


    public String actionBarDetailsTitle() {
        return actionBarDetailsTitle.getText();
    }

    @AndroidFindBy(id = BasicSetup.APP_PACKAGE_NAME + ":id/content")
    AndroidElement settingsDetailsActionBar;


    public void tapSettingsDetailsActionBar() {
        waitUtilities.waitElementToBecomeDisplayed(settingsDetailsActionBar, driver);
        settingsDetailsActionBar.click();
    }

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]")
    AndroidElement tomorrow;


    public void clickTomorrow () {
        waitUtilities.waitElementToBecomeClickable(tomorrow, driver);
        tomorrow.click();
    }

    @AndroidFindBy(id = BasicSetup.APP_PACKAGE_NAME + ":id/action_bar_container")
    AndroidElement actionBarContainer;

    public int sizeSettings() {
        waitUtilities.waitElementToBecomeDisplayed(actionBarContainer, driver);
        return driver.findElementsById(BasicSetup.APP_PACKAGE_NAME + ":id/action_bar_container").size();
    }

    @AndroidFindBy(id = BasicSetup.APP_PACKAGE_NAME + ":id/content")
    AndroidElement content;
    public int sizeContent() {
        waitUtilities.waitElementToBecomeDisplayed(content, driver);
        return driver.findElementsById("android:id/content").size();
    }

    @AndroidFindBy(id = "android:id/title")
    AndroidElement title;

    public String getTextFromTitle() {
        waitUtilities.waitElementToBecomeDisplayed(title, driver);
        return title.getText();
    }






}
